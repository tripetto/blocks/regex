## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 📦 Regular Expression Block
[![Version](https://badgen.net/npm/v/@tripetto/block-regex?icon=npm&label)](https://www.npmjs.com/package/@tripetto/block-regex)
[![Downloads](https://badgen.net/npm/dt/@tripetto/block-regex?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/block-regex)
[![License](https://badgen.net/npm/license/@tripetto/block-regex?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/block-regex)
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/blocks/stock/regex/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/blocks/regex/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

Condition block for Tripetto that uses regular expressions to validate a value.

## 📺 Preview
[![Preview](https://unpkg.com/@tripetto/block-regex/assets/preview.png)](https://codepen.io/tripetto/live/wvpLqXX/f1d46e995ad068ce7de024bbc84d6346)

[![Try the demo](https://unpkg.com/@tripetto/builder/assets/button-demo.svg)](https://codepen.io/tripetto/live/wvpLqXX/f1d46e995ad068ce7de024bbc84d6346)

## 🚀 Get started
You can find all the information to start with this block at [tripetto.com/sdk/docs/blocks/stock/regex/](https://tripetto.com/sdk/docs/blocks/stock/regex/).

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 🆘 Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/blocks/regex/issues).

Need help or assistance? Please go to our [support page](https://tripetto.com/sdk/support/). We're more than happy to help you.

## 💳 License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

## ✨ Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)
- [Julian Frauenholz](https://gitlab.com/frauenholz) (German translation)
- [Krzysztof Kamiński](https://gitlab.com/kriskaminski) (Polish translation)

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
