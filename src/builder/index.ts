/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "@tripetto/builder";
import { IRegExCondition } from "../runner";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: "*",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:regex", "Regular expression");
    },
})
export class RegExCondition extends ConditionBlock implements IRegExCondition {
    @definition
    @affects("#name")
    variable?: string;

    @definition
    regex = "";

    @definition
    invert = false;

    // Return an empty label, since the variable name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        return (
            (this.variable && `${this.type.label} @${this.variable}`) ||
            this.type.label
        );
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:regex", "Input"),
            controls: [
                new Forms.Dropdown(
                    populateVariables(this, undefined, this.variable),
                    this.variable
                )
                    .placeholder(
                        pgettext(
                            "block:regex",
                            "Select the input variable to use..."
                        )
                    )
                    .on((variable) => {
                        this.variable = variable.value || undefined;
                        this.slot =
                            (variable.value &&
                                lookupVariable(this, variable.value)?.slot) ||
                            undefined;
                    })
                    .autoFocus(),
            ],
        });

        this.editor.form({
            title: pgettext("block:regex", "Regular expression"),
            controls: [
                new Forms.Text("singleline", Forms.Text.bind(this, "regex", ""))
                    .placeholder(
                        pgettext(
                            "block:regex",
                            "Regex literal (for example /ab+c/)"
                        )
                    )
                    .autoValidate((regex) => {
                        if (!regex.value) {
                            return "unknown";
                        }

                        try {
                            const literalSignLeft = regex.value.indexOf("/");
                            const literalSignRight =
                                regex.value.lastIndexOf("/");

                            return literalSignLeft === 0 &&
                                literalSignRight > literalSignLeft &&
                                ((r: string, n: number) => {
                                    try {
                                        return (
                                            new RegExp(
                                                r.substring(1, n),
                                                r.substr(n + 1)
                                            ) instanceof RegExp
                                        );
                                    } catch {
                                        return false;
                                    }
                                })(regex.value, literalSignRight)
                                ? "pass"
                                : "fail";
                        } catch {
                            return "fail";
                        }
                    })
                    .enter(this.editor.close)
                    .escape(this.editor.close),
                new Forms.Checkbox(
                    pgettext("block:regex", "Invert regular expression"),
                    Forms.Checkbox.bind(this, "invert", false)
                ),
            ],
        });
    }
}
