/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

export interface IRegExCondition {
    readonly variable?: string;
    readonly regex: string;
    readonly invert: boolean;
}

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class RegExCondition extends ConditionBlock<IRegExCondition> {
    @condition
    validate(): boolean {
        if (this.props.variable) {
            const variable = this.variableFor(this.props.variable);

            if (variable) {
                try {
                    const regex = this.props.regex || "";
                    const literalSignLeft = regex.indexOf("/");
                    const literalSignRight = regex.lastIndexOf("/");

                    return (
                        literalSignLeft === 0 &&
                        literalSignRight > literalSignLeft &&
                        ((v: string) => {
                            try {
                                return (
                                    new RegExp(
                                        regex.substring(1, literalSignRight),
                                        regex.substr(literalSignRight + 1)
                                    ).test(v) ===
                                    (this.props.invert ? false : true)
                                );
                            } catch {
                                return false;
                            }
                        })(variable.string)
                    );
                } catch {
                    return false;
                }
            }
        }

        return false;
    }
}
